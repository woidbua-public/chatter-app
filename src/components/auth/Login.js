import M from "materialize-css";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { withFirebase } from "react-redux-firebase";
import { compose } from "redux";
import { signIn } from "../../store/actions/authActions";

export class Login extends Component {
    static propTypes = {
        authError: PropTypes.string,
        signIn: PropTypes.func
    };

    state = {
        email: "",
        password: ""
    };

    componentDidMount = () => {
        M.AutoInit();
    };

    handleChange = (e) => {
        const target = e.target;
        this.setState(() => ({
            [target.id]: target.value
        }));
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.signIn(this.state);
    };

    render() {
        const { authError } = this.props;

        return (
            <div className="container">
                <form className="white" onSubmit={this.handleSubmit}>
                    <h5 className="grey-text text-darken-3">Login</h5>
                    <div className="input-field">
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            id="email"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="input-field">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            id="password"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="input-field">
                        <button className="btn orange z-depth-0">Login</button>
                        {authError && (
                            <div className="red-text center">{authError}</div>
                        )}
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        authError: state.auth.authError
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        signIn: (credentials) => dispatch(signIn(ownProps, credentials))
    };
};

export default compose(
    withFirebase,
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(Login);
