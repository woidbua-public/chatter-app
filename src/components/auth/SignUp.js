import M from "materialize-css";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { withFirebase, withFirestore } from "react-redux-firebase";
import { compose } from "redux";
import { signUp } from "../../store/actions/authActions";

export class SignUp extends Component {
    static propTypes = {
        auth: PropTypes.object,
        authError: PropTypes.string,
        signUp: PropTypes.func
    };

    state = {
        email: "",
        password: "",
        firstName: "",
        lastName: ""
    };

    componentDidMount = () => {
        M.AutoInit();
    };

    handleChange = (e) => {
        const target = e.target;
        this.setState(() => ({
            [target.id]: target.value
        }));
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.signUp(this.state);
    };

    render() {
        const { authError } = this.props;

        return (
            <div className="container">
                <form className="white" onSubmit={this.handleSubmit}>
                    <h5 className="grey-text text-darken-3">Sign Up</h5>
                    <div className="input-field">
                        <label htmlFor="firstName">First Name</label>
                        <input
                            type="text"
                            id="firstName"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="input-field">
                        <label htmlFor="lastName">Last Name</label>
                        <input
                            type="text"
                            id="lastName"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="input-field">
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            id="email"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="input-field">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            id="password"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="input-field">
                        <button className="btn orange z-depth-0">
                            Sign Up
                        </button>
                        {authError && (
                            <div className="red-text center">{authError}</div>
                        )}
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth,
        authError: state.auth.authError
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        signUp: (newUser) => dispatch(signUp(ownProps, newUser))
    };
};

export default compose(
    withFirebase,
    withFirestore,
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(SignUp);
