import PropTypes from "prop-types";
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import SignedInLinks from "./SignedInLinks";
import SignedOutLinks from "./SignedOutLinks";

const Navbar = (props) => {
    const { auth, profile } = props;

    const getInitials = () => {
        if (profile.firstName && profile.lastName) {
            return profile.firstName.charAt(0) + profile.lastName.charAt(0);
        } else if (auth.email) {
            return (
                auth.email.charAt(0) +
                auth.email.charAt(auth.email.indexOf("@") + 1)
            );
        }

        return null;
    };

    const links = auth.uid ? (
        <SignedInLinks initials={getInitials()} />
    ) : (
        <SignedOutLinks />
    );

    return (
        <nav className="nav-wrapper grey darken-3">
            <div className="container">
                <Link to="/" className="brand-logo">
                    Chatter App
                </Link>
                {links}
            </div>
        </nav>
    );
};

Navbar.propTypes = {
    auth: PropTypes.object,
    profile: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth,
        profile: state.firebase.profile
    };
};

export default connect(mapStateToProps)(Navbar);
