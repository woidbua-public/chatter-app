import PropTypes from "prop-types";
import React from "react";
import { connect } from "react-redux";
import { withFirebase } from "react-redux-firebase";
import { NavLink } from "react-router-dom";
import { compose } from "redux";
import { signOut } from "../../store/actions/authActions";

const SignedInLinks = (props) => {
    const { initials } = props;
    return (
        <ul className="right">
            <li>
                <NavLink to="/posts/create">New Post</NavLink>
            </li>
            <li>
                <a href="#signout" onClick={props.signOut}>
                    Logout
                </a>
            </li>
            <li>
                <NavLink to="/" className="btn btn-floating orange">
                    {initials}
                </NavLink>
            </li>
        </ul>
    );
};

SignedInLinks.propTypes = {
    initials: PropTypes.string,
    signOut: PropTypes.func
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        signOut: () => dispatch(signOut(ownProps))
    };
};

export default compose(
    withFirebase,
    connect(
        null,
        mapDispatchToProps
    )
)(SignedInLinks);
