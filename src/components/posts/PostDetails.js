import moment from "moment";
import PropTypes from "prop-types";
import React, { Fragment } from "react";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";

const PostDetails = (props) => {
    const { post } = props;
    return (
        <div className="container section post-details">
            <div className="card z-depth-0">
                {post ? (
                    <Fragment>
                        <div className="card-content">
                            <span className="card-title">{post.title}</span>
                            <p>{post.content}</p>
                        </div>
                        <div className="card-action grey lighten-4 grey-text">
                            <div>
                                Posted by {post.authorFirstName}{" "}
                                {post.authorLastName}
                            </div>
                            <div>
                                {moment(post.createdAt.toDate()).calendar()}
                            </div>
                        </div>
                    </Fragment>
                ) : (
                    <Fragment>
                        <div className="card-content center">
                            <p>Loading project...</p>
                        </div>
                    </Fragment>
                )}
            </div>
        </div>
    );
};

PostDetails.propTypes = {
    match: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
    const { id } = ownProps.match.params;
    const { posts } = state.firestore.data;
    const post = posts ? posts[id] : null;
    return {
        post
    };
};

export default compose(
    connect(mapStateToProps),
    firestoreConnect([{ collection: "posts" }])
)(PostDetails);
