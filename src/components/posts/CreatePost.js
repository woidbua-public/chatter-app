import M from "materialize-css";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { withFirestore } from "react-redux-firebase";
import { compose } from "redux";
import { createPost } from "../../store/actions/postActions";

export class CreatePost extends Component {
    static propTypes = {
        createPost: PropTypes.func.isRequired
    };

    state = {
        title: "",
        content: ""
    };

    componentDidMount = () => {
        M.AutoInit();
    };

    handleChange = (e) => {
        const target = e.target;
        this.setState(() => ({
            [target.id]: target.value
        }));
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.createPost(this.state);
        this.props.history.push("/");
    };

    render() {
        return (
            <div className="container">
                <form className="white" onSubmit={this.handleSubmit}>
                    <h5 className="grey-text text-darken-3">Create new post</h5>
                    <div className="input-field">
                        <label htmlFor="title">Title</label>
                        <input
                            type="text"
                            id="title"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="input-field">
                        <label htmlFor="content">Content</label>
                        <textarea
                            id="content"
                            className="materialize-textarea"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="input-field">
                        <button className="btn orange z-depth-0">Create</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
    createPost: (post) => dispatch(createPost(ownProps, post))
});

export default compose(
    withFirestore,
    connect(
        null,
        mapDispatchToProps
    )
)(CreatePost);
