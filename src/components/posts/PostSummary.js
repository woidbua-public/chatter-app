import moment from "moment";
import PropTypes from "prop-types";
import React from "react";

const PostSummary = ({ post }) => {
    return (
        <div className="card z-depth-0 post-summary">
            <div className="card-content grey-text text-darken-3">
                <span className="card-title">{post.title}</span>
                <p>{post.content}</p>
                <p className="grey-text">
                    {moment(post.createdAt.toDate()).calendar()}
                </p>
            </div>
        </div>
    );
};

PostSummary.propTypes = {
    post: PropTypes.object.isRequired
};

export default PostSummary;
