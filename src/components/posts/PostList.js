import PropTypes from "prop-types";
import React from "react";
import PostSummary from "./PostSummary";
import { Link } from "react-router-dom";

const PostList = ({ posts }) => {
    return (
        <div className="post-list section">
            {posts &&
                posts.map((post) => (
                    <Link to={"/posts/" + post.id} key={post.id}>
                        <PostSummary post={post} />
                    </Link>
                ))}
        </div>
    );
};

PostList.propTypes = {
    posts: PropTypes.array
};

export default PostList;
