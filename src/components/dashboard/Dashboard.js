import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";
import PostList from "../posts/PostList";
import Notifications from "./Notifications";

export class Dashboard extends Component {
    static propTypes = {
        notifications: PropTypes.array,
        posts: PropTypes.array
    };

    render() {
        const { posts, notifications } = this.props;

        return (
            <div className="dashboard container">
                <div className="row">
                    <div className="col s12 m6">
                        <PostList posts={posts} />
                    </div>
                    <div className="col s12 m5 offset-m1">
                        <Notifications notifications={notifications} />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    notifications: state.firestore.ordered.notifications,
    posts: state.firestore.ordered.posts
});

export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        {
            collection: "notifications",
            limit: 10,
            orderBy: ["time", "desc"]
        },
        {
            collection: "posts",
            limit: 10,
            orderBy: ["createdAt", "desc"]
        }
    ])
)(Dashboard);
