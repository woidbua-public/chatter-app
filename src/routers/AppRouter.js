import "materialize-css/dist/css/materialize.min.css";
import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import Login from "../components/auth/Login";
import SignUp from "../components/auth/SignUp";
import Dashboard from "../components/dashboard/Dashboard";
import Navbar from "../components/layout/Navbar";
import CreatePost from "../components/posts/CreatePost";
import PostDetails from "../components/posts/PostDetails";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";

const AppRouter = () => {
    return (
        <BrowserRouter>
            <div className="App">
                <Navbar />
                <Switch>
                    <PrivateRoute path="/" component={Dashboard} exact={true} />
                    <PrivateRoute path="/posts/create" component={CreatePost} />
                    <PrivateRoute path="/posts/:id" component={PostDetails} />
                    <PublicRoute path="/login" component={Login} />
                    <PublicRoute path="/signup" component={SignUp} />
                </Switch>
            </div>
        </BrowserRouter>
    );
};

export default AppRouter;
