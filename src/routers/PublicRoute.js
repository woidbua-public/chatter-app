import React from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router-dom";

export const PublicRoute = ({
    isAuthenticated,
    component: Component,
    ...rest
}) => (
    <Route
        {...rest}
        component={(props) =>
            !isAuthenticated ? <Component {...props} /> : <Redirect to="/" />
        }
    />
);

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.firebase.auth.uid
});

export default connect(mapStateToProps)(PublicRoute);
