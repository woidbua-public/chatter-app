export const ADD_POST = "ADD_POST";
export const CREATE_POST_ERROR = "CREATE_POST_ERROR";

export const createPost = (props, post) => {
    return (dispatch, getState) => {
        const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;

        props.firestore
            .collection("posts")
            .add({
                ...post,
                authorId: authorId,
                authorFirstName: profile.firstName,
                authorLastName: profile.lastName,
                createdAt: new Date()
            })
            .then(() =>
                dispatch({
                    type: ADD_POST,
                    post
                })
            )
            .catch((err) => {
                dispatch({ type: CREATE_POST_ERROR, err });
            });
    };
};
