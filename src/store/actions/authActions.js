export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const SIGNOUT_SUCCESS = "SIGNOUT_SUCCESS";
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
export const SIGNUP_ERROR = "SIGNUP_ERROR";

export const signIn = (props, credentials) => {
    return (dispatch, getState) => {
        props.firebase
            .auth()
            .signInWithEmailAndPassword(credentials.email, credentials.password)
            .then(() => {
                dispatch({ type: LOGIN_SUCCESS });
            })
            .catch((err) => {
                dispatch({ type: LOGIN_ERROR, err });
            });
    };
};

export const signOut = (props) => {
    return (dispatch, getState) => {
        props.firebase
            .auth()
            .signOut()
            .then(() => {
                dispatch({ type: SIGNOUT_SUCCESS });
            });
    };
};

export const signUp = (props, newUser) => {
    return (dispatch, getState) => {
        props.firebase
            .auth()
            .createUserWithEmailAndPassword(newUser.email, newUser.password)
            .then((resp) => {
                return props.firestore
                    .collection("users")
                    .doc(resp.user.uid)
                    .set({
                        firstName: newUser.firstName,
                        lastName: newUser.lastName
                    });
            })
            .then(() => {
                dispatch({ type: SIGNUP_SUCCESS });
            })
            .catch((err) => {
                dispatch({ type: SIGNUP_ERROR, err });
            });
    };
};
