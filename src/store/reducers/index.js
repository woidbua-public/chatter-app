import { firebaseReducer } from "react-redux-firebase";
import { combineReducers } from "redux";
import { firestoreReducer } from "redux-firestore";
import authReducer from "./authReducer";
import postsReducer from "./postsReducer";

const reducers = combineReducers({
    auth: authReducer,
    posts: postsReducer,
    firebase: firebaseReducer,
    firestore: firestoreReducer
});

export default reducers;
