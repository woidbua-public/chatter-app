import { ADD_POST, CREATE_POST_ERROR } from "../actions/postActions";

const initState = {
    posts: [
        { id: 1, title: "First post!", content: "blah blah blah" },
        { id: 2, title: "Second post!", content: "blah blah blah" },
        { id: 3, title: "Third post!", content: "blah blah blah" }
    ]
};

const postsReducer = (state = initState, action) => {
    switch (action.type) {
        case ADD_POST:
            console.log("created post", action.post);
            return state;
        case CREATE_POST_ERROR:
            console.log("create post error", action.err);
            return state;
        default:
            return state;
    }
};

export default postsReducer;
