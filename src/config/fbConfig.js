import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

var config = {
    apiKey: "AIzaSyBomL7-GmCfF2TbI0BeydAxG18KjwmqVkw",
    authDomain: "chatter-app-85ad3.firebaseapp.com",
    databaseURL: "https://chatter-app-85ad3.firebaseio.com",
    projectId: "chatter-app-85ad3",
    storageBucket: "chatter-app-85ad3.appspot.com",
    messagingSenderId: "200607224139"
};

firebase.initializeApp(config);
firebase.firestore();

export default firebase;
