import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ReactReduxFirebaseProvider } from "react-redux-firebase";
import { applyMiddleware, compose, createStore } from "redux";
import { createFirestoreInstance } from "redux-firestore";
import thunk from "redux-thunk";
import Loader from "./components/layout/Loader";
import firebase from "./config/fbConfig";
import AppRouter from "./routers/AppRouter";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import reducers from "./store/reducers";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));

const rrfConfig = {
    userProfile: "users",
    useFirestoreForProfile: true
};

const rrfProps = {
    firebase,
    config: rrfConfig,
    dispatch: store.dispatch,
    createFirestoreInstance
};

const jsx = (
    <Provider store={store}>
        <ReactReduxFirebaseProvider {...rrfProps}>
            <AppRouter />
        </ReactReduxFirebaseProvider>
    </Provider>
);

ReactDOM.render(<Loader />, document.getElementById("root"));

firebase.auth().onAuthStateChanged(() => {
    ReactDOM.render(jsx, document.getElementById("root"));
    serviceWorker.unregister();
});
