<!-- markdownlint-disable MD030-->

# Chatter-App

A simple app where users can post messages.
Additionally, the most recent actions will be displayed as notifications (e.g. new user, new message, ...).

## App

See the app in action: [https://chatter-app-85ad3.firebaseapp.com/](https://chatter-app-85ad3.firebaseapp.com/)

### Login / Signup

![Dashboard](./docs/img/login_signup.png)

### Dashboard

![Dashboard](./docs/img/dashboard.jpg)

### Create Post

![Dashboard](./docs/img/create_post.png)

## Implementation

### Client / Browser

-   React App
-   Redux

### Server / Firebase

-   Firestore db
-   Firebase Auth
-   Cloud Functions

## Commands

### Firebase CLI

-   `firebase login`
-   `firebase init`
-   `firebase deploy --only functions`
